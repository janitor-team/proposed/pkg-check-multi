nagios-plugin-check-multi (0.26-5) UNRELEASED; urgency=medium

  * Team upload.
  * Bump Standards-Version to 4.5.1, no changes.
  * Update watch file for GitHub URL changes.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 28 Nov 2020 14:33:21 +0100

nagios-plugin-check-multi (0.26-4) unstable; urgency=medium

  * Team upload.

  [ Bas Couwenberg ]
  * Bump Standards-Version to 4.5.0, changes:
    - Use priority optional instead of extra
  * Bump debhelper compat to 10, changes:
    - Don't explicitly enable autoreconf, enabled by default
    - Drop dh-autoreconf build dependency
  * Update watch file to use GitHub releases.
  * Bump watch file version to 4.
  * Use pkg-info.mk instead of dpkg-parsechangelog.
  * Add lintian override for spare-manual-page.
  * Add gbp.conf to use pristine-tar & --source-only-changes by default.

  [ Marc Haber ]
  * new salsa URLs and new github Homepage in debian/control

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 20 Nov 2020 10:00:32 +0100

nagios-plugin-check-multi (0.26-3.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix "FTBFS with Perl 5.26: Unescaped left brace in regex is
    deprecated here": update unescaped-left-brace-in-regex.patch to escape one
    more '{'. (Closes: #865888)

 -- gregor herrmann <gregoa@debian.org>  Fri, 14 Jul 2017 22:42:46 +0200

nagios-plugin-check-multi (0.26-3) unstable; urgency=medium

  * Team upload.
  * Add patch to add test script directory perl search path.
  * Add patch to fix 'Unescaped left brace in regex is deprecated' warning.
  * Add patch to fix 10_check_multi.t failure.
    (closes: #808737)
  * Include patches from separate patch branches.
  * Restructure control file with cme.
  * Update Vcs-* URLs to use HTTPS.
  * Update copyright file using copyright-format 1.0.
  * Fix unknown & unused substitution variables.
  * Improve extended description.
  * Drop build dependency on autotools-dev, pulled in via dh-autoreconf.
  * Drop unused build dependency on dctrl-tools.
  * Bump debhelper compatibility to 9.
  * Bump Standards-Version to 3.9.8, changes: copyright-format 1.0.
  * Update configure options for icinga paths.
  * Update watch file to handle common issues.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 24 Dec 2016 14:24:12 +0100

nagios-plugin-check-multi (0.26-2) unstable; urgency=low

  * Add patch to fix FTBFS with perl 5.18 (Closes: #724113)
  * Add patch to ensure "make all" does not overwrite local git hooks
  * Bump standards version (no changes)
  * Remove version from help2man build dependency
  * Use dh-autoreconf
  * Use "make all" to build configuration properly

 -- Stig Sandbeck Mathisen <ssm@debian.org>  Sun, 29 Sep 2013 13:47:54 +0200

nagios-plugin-check-multi (0.26-1) unstable; urgency=low

  * New upstream release (Closes: #642780)
    - Remove "more specific grep" patch, included upstream
    - Remove ./configure argument "--docdir"
  * Add a debian/watch file, for uscan
  * Remove debhelper template(s)

 -- Stig Sandbeck Mathisen <ssm@debian.org>  Sat, 26 Nov 2011 20:28:20 +0100

nagios-plugin-check-multi (0.25-1) unstable; urgency=low

  * New upstream version
  * Use a more specific grep in tests (Closes: #628351)
  * Update VCS-* headers to point to anonscm.debian.org
  * Unapply patches after build
  * Bump standards-version (no changes)

 -- Stig Sandbeck Mathisen <ssm@debian.org>  Sat, 04 Jun 2011 13:47:17 +0200

nagios-plugin-check-multi (0.24-2) unstable; urgency=low

  [ Stig Sandbeck Mathisen ]
  * Build with defaults for nagios3; add a README.Debian for Icinga
    instructions

  [ Marc Haber ]
  * add plugin example configuration

 -- Stig Sandbeck Mathisen <ssm@debian.org>  Thu, 27 Jan 2011 11:19:58 +0100

nagios-plugin-check-multi (0.24-1) unstable; urgency=low

  [ Stig Sandbeck Mathisen ]
  * Initial release (Closes: #601966)

  [ Marc Haber ]
  * help rules clean cleaning up completely
  * add versioned dependency for help2man

 -- Stig Sandbeck Mathisen <ssm@debian.org>  Sun, 21 Nov 2010 19:13:17 +0100
